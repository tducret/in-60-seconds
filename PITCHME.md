# Let's Get Started

---

## Add Some Slide Candy

![](assets/img/presentation.png)

---
@title[Customize Slide Layout]

@snap[west span-50]
## Customize Slide Content Layout
@snapend

@snap[east span-50]
![](assets/img/presentation.png)
@snapend

---?color=#E58537
@title[Add A Little Imagination]

@snap[north-west]
#### Add a splash of @color[cyan](**color**) and you are ready to start presenting...
@snapend

@snap[west span-55]
@ul[spaced text-white]
- You will be amazed
- What you can achieve
- *With a little imagination...*
- And **GitPitch Markdown**
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/conference.png)
@snapend

---?image=assets/img/presenter.jpg

@snap[north span-100 headline]
## Now It's Your Turn
@snapend

@snap[south span-100 text-06]
[Click here to jump straight into the interactive feature guides in the GitPitch Docs @fa[external-link]](https://gitpitch.com/docs/getting-started/tutorial/)
@snapend

---
@title[Zoom-Out Example]

@snap[west span-40]
@code[golang zoom-07](src/go/sample.go)
@snapend

@snap[east montserrat-heavy span-50]
### Live Code Presenting
@snapend

@snap[north-east span-100 text-05]
To _**zoom-out**_ on a code block, use **&#64;code shortcut syntax** and one of the **built-in zoom** CSS styles.<br>This approach gives you a nice way way to introduce code on upcoming slides.
@snapend

